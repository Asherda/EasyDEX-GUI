# EasyDEX-GUI
Agama GUI from SuperNETorg/EasyDEX-GUI via KomodoPlatform/EasyDEX-GUI.
## Building
Change into the react directory and install and build:
```bash
git clone https://github.com/VerusCoin/EasyDEX-GUI.git
cd EasyDEX-GUI/react
npm install
npm run build
```
This creates the files needed for Agama in the EasyDEX-GUI/react/build directory.
